﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackSystem : MonoBehaviour
{
    int comboNum = 0;
    bool canDoCombo = false;
    Animator anim;
    private void Start()
    {
        anim = GetComponentInChildren<Animator>();
    }
    public void Attack()
    {
        if (comboNum <= 3)
            comboNum += 1;
        else
            comboNum = 1;
        if(canDoCombo == false) //первая атака
        {
            anim.SetInteger("Combo", 1);
        }
        else //о, я могу сделать комбо
        {
            anim.SetInteger("Combo", comboNum);
        }
    }

    #region shittyCode
    public void OpenComboWindow()
    {
        canDoCombo = true;
    }
    public void CloseComboWindow()
    {
        canDoCombo = false;
    }
    #endregion
}
